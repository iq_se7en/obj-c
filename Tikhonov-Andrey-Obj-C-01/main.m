//
//  main.m
//  Tikhonov-Andrey-Obj-C-01
//
//  Created by Андрей Тихонов on 20.09.2018.
//  Copyright © 2018 Андрей Тихонов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
